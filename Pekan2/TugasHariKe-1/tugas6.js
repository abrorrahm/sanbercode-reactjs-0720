//Soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var objDaftarPeserta = {
    nama : "Asep",
    jenisKelamin: "laki-laki",
    hobi: "baca buku",
    tahunLahir: 1992
}
console.log(objDaftarPeserta)

//Soal 2
var dataBuah = [{nama: "strawberry", warna: "merah", adaBijinya: "tidak", harga: 8000}, 
{nama: "jeruk", warna: "oranye", adaBijinya: "ada", harga: 8000}, 
{nama: "Semangka", warna: "Hijau & Merah", adaBijinya: "ada", harga: 10000},
{nama: "Pisang", warna: "Kuning", adaBijinya: "tidak", harga: 5000}]
console.log(dataBuah[0])

//Soal 3
var dataFilm = []
var tambahFilm = {
    nama : "IT",
    durasi : "2 Jam",
    genre : "Horror",
    tahun : 2019,
  }
  function tambahkanFilm(tambahFilm){
      dataFilm.push(tambahFilm)
  }
  tambahkanFilm(tambahFilm)
  console.log(dataFilm)
  
//Soal 4
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name(){
        return this._name;
    }

    set name(x){
        this._name = x;
    }

    get legs(){
        return this._legs;
    }

    set legs(x){
        this._legs = x;
    }

    get cold_blooded(){
        return this._cold_blooded;
    }

    set cold_blooded(x){
        this._cold_blooded = x;
    }
}
var sheep = new Animal("shaun");
 
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded) 

class Ape extends Animal{
    constructor(name) {
        super(name)
        super.legs = 2
    }

    yell(){
       console.log ("Auooo")
    }
}

class Frog extends Animal{
    constructor(name) {
        super(name)
        super._legs
    }

    jump(){
        console.log ("hop hop")
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
console.log(sungokong.name)
console.log(sungokong.legs)
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok.name)
console.log(kodok.legs) 

//Soal 5
class Clock {
    constructor({template}){
        this.template = template;

        var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };
    }

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  