//Soal 1
const luasLingkaran = (r) => {
    let luas = 3.14 * r * r;
    return luas;
};
console.log(luasLingkaran(7));

let kelilingLingkaran = (r) => {
    const keliling = 2 * 3.14 * r;
    return keliling;
};
let keliling = kelilingLingkaran(5)
console.log (keliling)

//Soal 2
let kalimat = ""

const kataKata = (kalimat) => {
    let kata1 = "saya"
    let kata2 = "adalah"
    let kata3 = "seorang"
    let kata4 = "frontend"
    let kata5 = "developer"
    kalimat =  `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
    return kalimat;
}
console.log(kataKata`${kalimat}`);


//Soal 3
class Books {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }

    tampilkan(){
        return `Name :${this.name}, Total Page :${this.totalPage}, Price :${this.price}`
    }
}

class Komik extends Books{
    constructor(name, totalPage, price, isColorful){
        super(name, totalPage, price)
        this.isColorful = isColorful;
    }
    tampilkan(){
        return `Name :${this.name}, Total Page :${this.totalPage}, Price :${this.price}, Is Colorful : ${this.isColorful}`
    }
}

let buku = new Books("Tematik", 500, 100000);
console.log(buku.tampilkan());

let komik = new Komik("One Piece", 100, 30000, false);
console.log(komik.tampilkan());