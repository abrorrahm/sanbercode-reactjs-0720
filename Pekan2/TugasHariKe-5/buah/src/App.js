import React from 'react';
import './App.css';

function App() {
    return (
        <div className="container">
            <h1>Form Pembelian Buah</h1>
            <table>
                <tr>
                    <th>Nama Pelanggan</th>
                    <td><input type="text"/></td>
                </tr>
                <tr>
                    <th id="daftar">Daftar Item</th>
                    <td>
                        <input type="checkbox" name="buah"/> Semangka <br/>
                        <input type="checkbox" name="buah"/> Jeruk <br/>
                        <input type="checkbox" name="buah"/> Nanas <br/>
                        <input type="checkbox" name="buah"/> Salak <br/>
                        <input type="checkbox" name="buah"/> Anggur <br/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <button value="Kirim" type="button">Kirim</button>
                </tr>
            </table>
        </div>
    );
}

export default App;
