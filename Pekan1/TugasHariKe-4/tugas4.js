//Soal 1
var love = 2;
console.log("LOOPING PERTAMA");
while(love <= 20) { 
  console.log(love + " - I love coding"); 
  love += 2; 
}

var frontend = 20;
console.log("LOOPING KEDUA");
while(frontend >= 2) { 
  console.log(frontend + " - I will become a frontend developer"); 
  frontend -= 2; 
}

//Soal 2
for(var angka = 1; angka <= 20; angka++) {
    if((angka%3)===0 && ((angka%2)===1)){
      console.log(angka + ' - I Love Coding');
    }
    else if((angka%2)==1){
      console.log(angka + ' - Santai');
    }
    else if ((angka%2)===0) {
      console.log(angka + ' - Berkualitas');
    }
} 

//Soal 3
var x = "#";
for(var i = 1; i <= 7; i++){
    var y = "";
    for(var j = 1; j <= i; j++){
        y += x;
    }
    console.log(y);
}

//Soal 4
var kalimat = "saya sangat senang belajar javascript"
var x = kalimat.split(" ")
console.log(x)

//Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
for (var i = 0; i <= 4; i++){
    console.log(daftarBuah[i])
}
