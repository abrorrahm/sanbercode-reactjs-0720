// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
console.log(kataPertama, capitalizeFirstLetter("senang"), kataKetiga, kataKeempat.toUpperCase());

// Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var satu = parseInt(kataPertama)
var dua = parseInt(kataKedua)
var tiga = parseInt(kataKetiga)
var empat = parseInt(kataKeempat)

console.log(satu + dua + tiga + empat)

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Soal 4
var nilai = 55;

// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E

if (nilai >= 80 ){
  console.log("Indeksnya A")
}else if (nilai >= 70 && nilai < 80){
  console.log("Indeksnya B")
}else if (nilai >= 60 && nilai < 70){
  console.log("Indeksnya C")
}else if (nilai >= 50 && nilai < 60){
  console.log("Indeksnya D")
}else{
  console.log("E")
}

// Soal 5
var tanggal = 17;
var bulan = 10;
var tahun = 1997;

switch(bulan){
  case 1: {
    console.log("17 Januari 1997")
    break;
  }
  case 2: {
    console.log("17 Februari 1997")
    break;
  }
  case 3: {
    console.log("17 Maret 1997")
    break;
  }
  case 4: {
    console.log("17 April 1997")
    break;
  }
  case 5: {
    console.log("17 Mei 1997")
    break;
  }
  case 6: {
    console.log("17 Juni 1997")
    break;
  }
  case 7: {
    console.log("17 Juli 1997")
    break;
  }
  case 8: {
    console.log("17 Agustus 1997")
    break;
  }
  case 9: {
    console.log("17 September 1997")
    break;
  }
  case 10: {
    console.log("17 Oktober 1997")
    break;
  }
  case 11: {
    console.log("17 November 1997")
    break;
  }
  case 12: {
    console.log("17 Desember 1997")
    break;
  }
}

